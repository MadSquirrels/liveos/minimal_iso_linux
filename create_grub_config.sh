#!/bin/sh

#
# $1 = Name
# $2 = kernel_path
# $3 = ramfs_path
# $4 = output_directory
#
mkdir -p "$4"/boot/grub/
cat <<EOF > "$4"/boot/grub/grub.cfg
menuentry "$1" {
	linux /$2 root=/ loglevel=3 #quiet
        initrd /$3
}
EOF

