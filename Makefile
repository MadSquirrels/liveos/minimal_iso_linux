###################################
#                                 #
# Script to make a minimal linux  #
#                                 #
###################################


# tuto to upgrade the script https://wiki.gentoo.org/wiki/Custom_Initramfs


# download source linux
# make mrproper
# make defconfig or menuconfig to have a specific kernel
# make -j"$(nproc)"
# cp arch/x86/boot/bzImage $(OUT)/bzImage

KERNEL:=bzImage

# only one binarie
#INITRD=initrd/

INITRD:=initrd_archlinux/

ISO:=test.iso
CPIO:=rootfs.cpio.gz

OUT:=test
ifeq ($(NAME),)
NAME:="Minimal Linux"
endif


all:initrd grub

initrd:
	make -C $(INITRD)
	cd $(INITRD) && find . | cpio -o -H newc | gzip > ../$(OUT)/$(CPIO)

grub:grub_conf
	grub-mkrescue -o test.iso $(OUT)/

run:
	qemu-system-x86_64 -m 4G  -cdrom $(ISO)

no_grub:
	qemu-system-x86_64 -kernel $(OUT)/bzImage -initrd $(OUT)/$(CPIO)

grub_conf:
	./create_grub_config.sh $(NAME) $(KERNEL) $(CPIO) $(OUT)

clean:
	make -C $(INITRD) clean
	$(RM) $(ISO) $(OUT)/$(CPIO) 
